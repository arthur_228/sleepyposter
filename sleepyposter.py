########################CHANGE YOUR SETTINGS HERE########################
CLASSIC_CLIPPER =   "arthur_228"
CLASSIC_EDITTOR =   "arthur_228"

CLASSIC_POST_TIME = "now" #<< "now" or "13:40" format

###### yt doesn't like overtagging => similar tags reduced. has less zoomer audience => non-zoomer terms ######
YT_CLASSIC_TAGS =	"#shorts #vtuber #anime #catgirl #comfy #vtuberclips #smallstreamer #bestof #highlights"

###### tt seem to not care about tags amount => repetitive tags. has zoomer audience => zoomer terms ######
TT_CLASSIC_TAGS =	"#vtuber #envtuberclip #envtuber #envtuberclips #streamerclips #english #twitch #anime #catgirl #comfy #smallstreamer #twitchclips #fyp #fypシ #foryoupage #fypage"

#########################################################################
import re
import pyperclip

_collapse = True

def query(prompt, def_value):
	if def_value == "":
		u_def_value = "skip"
	else:
		u_def_value = def_value
	res = input(f"{prompt} [{u_def_value}] ")
	if res == "":
		res = def_value
	return res

def tagify(m_str, collapse = False):
	if m_str == "":
		return m_str
	res = m_str.lower()
	if collapse:
		res = "#"+re.sub("[^a-z0-9]+", "", res)
		return res
	else:
		taglist = re.sub("[^a-z0-9\ _\-]+","",res).split(" ")
		res = ""
		for item in taglist:
			if item.strip() != "":
				res += " #" + item
		return res.strip()

#MAIN
print("ⓘ Welcome to sleepyposter!\n\
ⓘ your default options will be in brackets [like so]\n\
you can just hit enter to choose them ^\n\
ⓘ also recommend using universal shortcuts\n\
  Ctrl+A to select text in the field\n\
  Ctrl+V to paste text you'll get in your clipboard\n\n")

post_time = query("> are you scheduling your short on yt or posting now? (time format \"hh:mm\" or \"now\")",CLASSIC_POST_TIME)

tt_name = query("> how should we name this clip on tiktok?","")
yt_name = query("> how should we name this clip on yt?",tt_name)

clipper = query("> who clipped it?",CLASSIC_CLIPPER)
edittor = query("> who editted the clip?", CLASSIC_EDITTOR)
if clipper == edittor:
	credits = f"clipped & editted by {clipper}!"
else:
	credits = f"clipped by {clipper}, editted by {edittor}!"

collab_tags = tagify(query("> collabed with someone to tag",""))
game_tag = tagify(query("> what game to tag in the clip?",""), _collapse)
extra_tags = tagify(query(f"✓ your additional tags are: {collab_tags} {game_tag}\n> add more?",""))

pinned_comm = "🍮check me out on twitch here if u want! :)\n\
🔗 https://www.twitch.tv/sleepypurin\n\
🔴LIVE on weekdays 1pm CEST | 3am WHATEVER\n\
📅updated schedule:\n\
🔗 https://www.twitch.tv/sleepypurin/schedule\n"


#YT PRINTER
print(f"\n✓ paste to yt title")
pyperclip.copy(yt_name)
input("> Hit Enter to continue...")

print(f"\n✓ paste to yt description")
pyperclip.copy(f"{credits}\n\n{pinned_comm}\n\n{YT_CLASSIC_TAGS} {collab_tags} {game_tag} {extra_tags}".strip())
input("> Hit Enter to continue...")

#add dictionary+smile dictionary
if post_time == "now":
	print("\n✓ publish your video, go to it and ..")
	print("✓ paste to yt pinned comment")
	pyperclip.copy(pinned_comm)
else:
	print("\n✓ proceed to Visibility step, select schedule, pick correct date and ..")
	print("✓ paste to your schedule time field")
	pyperclip.copy(post_time)
	print("\n/!\\ don't forget to copy comment from description when it goes public")
input("> Hit Enter to continue...")

#TT PRINTER
print("\n✓ paste to tiktok caption")
pyperclip.copy(f"{tt_name}\n{TT_CLASSIC_TAGS} {collab_tags} {game_tag} {extra_tags}".strip() +"\n"+ credits)
input("> Hit Enter to finish...")

input("\n> lmk if there can be any improvements/QoL updates/corrections")