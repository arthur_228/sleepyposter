# sleepyposter

## What is it and how does it work?
It's a little script to help you post clips minimizing thought process and making it quicker and more intuitive

## Installation
This script is a Python code, so first you need Python on your PC, thus (if you haven't done it yet):

>1.1 Download it from official site
 [https://www.python.org/downloads/](https://www.python.org/downloads/)

>1.2 Launch installer: you can pick "Install Now" if you don't want to think extra time

If you have Python now, you just need to get `pyperclip` package:

>2.1 Open terminal (can search for it in start menu, or use shortcut `WIN+R`, type `cmd.exe` in the field and hit Enter)

>2.2 Paste this in the terminal you just opened: `python.exe -m pip install pyperclip`

You just installed library that allows this script to work with your clipboard (aka update text you're gonna paste)!

Now download actual script from here, for that
> 3 Click on `sleepyposter.py` right above and hit lil download button (on top left). You can check code itself here as well if you're curious 


## Using the script
Quite easy process: to launch the script double-click `sleepyposter.py` wherever you downloaded it to (open with Python if it asks for some reason.) Answer the prompts in openned window, and then follow instructions where to paste created text.
## Updating default settings
To update default settings, open `sleepyposter.py` with notepad (or any preferred text editor) and rewrite whatever you need in "###CHANGE YOUR SETTINGS HERE###" section

## Future plans (when the need is here)
- dork-proof the script
- improve pinned comment
- update collab section?